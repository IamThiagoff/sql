-- Constraints --
-- Pode dar um nome ou o oracle da NOT NULL --
create table aluno
(
  id NUMBER(6) not null,
  codigo varchar2(20) not null,
  nome varchar2(100) not null,
  id_disiplina NUMBER(4) not null 
);
-- PRIMARY KEY
  --Nivel de coluna
 create table AlunoPK(
  ID NUMBER(6) NOT NULL CONSTRAINT PKID PRIMARY KEY,
  codigo varchar2(20) not null,
  nome varchar2(100) not null,
  id_disiplina number(4) not null,
  CREATION_DATE date default sysdate not null,
  start_date date
 );
-- Nivel de tabela
 create table AlunoPK2 (
  ID number(6) not null,
  codigo varchar2(20) not null,
  nome varchar2(100) not null,
  id_disiplina number(4) not null,
  CREATION_DATE date default sysdate not null,
  start_date date,
  constraint PKID primary key(ID) 
 );
-- Unique 
-- Nível de Coluna
create table AnunoUNQ(
  ID number(6) not null constraint IDPK Primary key,
  codigo varchar2(10) not null constraint UKCode Unique,
  nome varchar2(100) not null,
  id_disiplina number(4) not null,
  CREATION_DATE date  default sysdate not null,
);
-- Nivel de tabela
create table AlunoUNQ2(
  ID number(6) not null,
  codigo varchar 2(10) not null,
  nome varchar2(100) not null,
  id_disiplina number(4) not null,
  CREATION_DATE date default sysdate not null,
  constraint IDPK primary key(ID),
  constraint UKCode Unique (Codigo) 
);